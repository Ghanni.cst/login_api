const express = require ('express');
const app = express ();
const bcrypt = require('bcrypt');
const { error } = require('console');

app.use(express.json()) //allows us to accept json



const users=[]

app.get ('/users',async (req,res)=>{
    res.json (users);
});

app.post('/users',async(req,res)=>{
    try{
        const hashPassword=await bcrypt.hash(req.body.password,10) //10 is the default salt value
        const user = { name:req.body.name , password:hashPassword}
        users.push(user)
        res.status(200).send()
    }
    catch{
        res.status(500).send(error)
    }
});

app.post('/users/login',async(req,res)=>{
    const user = users.find(user=>user.name=req.body.name)
    if(user==null){
        return res.status(400).send('User does not exsits')
        }
        try{
            if (await bcrypt.compare(req.body.password ,user.password)){
                res.send('Success');
            }else{
                res.send("Not Allowed");
            }
        } 
        catch{
            res.status(500).send(error)
        }
});



 

const port = process.env.PORT || 3000;  //process.env.PORT is responsible for running web page on given port
app.listen(port, () => console.log(`Listening on port ${port}...`));


